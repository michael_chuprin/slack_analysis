import os
import json_reader.config

def get_all_paths(path_to_downloads):

    all_filenames = os.listdir(path_to_downloads)

    all_filepaths = [os.path.join(path_to_downloads, f) for f in all_filenames]

    return all_filepaths

def main():

    downloads_path = os.path.join(json_reader.config.FOLDERPATH_INTERMEDIATE_FILES, 'url_downloads')

    all_paths = get_all_paths(downloads_path)

    for x in all_paths:
        print(x)




if __name__ == '__main__':
    main()