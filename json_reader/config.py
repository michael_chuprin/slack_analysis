import os

FOLDERPATH_INPUT_FILES        = '/Users/michaelchuprin/pycharmprojects/slack_analysis/files/input'
FILENAME_SLACK_EXPORT         = 'Trading Slack export Dec 21 2016'

FOLDERPATH_INTERMEDIATE_FILES = '/Users/michaelchuprin/pycharmprojects/slack_analysis/files/intermediate'
FOLDERPATH_OUTPUT_FILES       = '/Users/michaelchuprin/pycharmprojects/slack_analysis/files/output'



CHANNELS = ['sp', 'crude', 'tbonds']

print('PATH_TO_SLACK_EXPORT found : {}'.format(os.path.exists(os.path.join(FOLDERPATH_INPUT_FILES, FILENAME_SLACK_EXPORT))))
