import os
import json
import time
import datetime
import urllib.request
import json_reader.config


def parse_username(username):
    if '|' in username:
        name = username.split('|')[1][:-1]

    else:
        name = username

    return name

def get_links_within_json(json_path):

    all_urls_in_this_json = []

    with open(json_path) as data_file:
        data = json.load(data_file)[0]


    for x in data:
        if data.get('file'):
            user       = parse_username(data.get('username', 'ANON'))
            url_path   = data.get('file').get('url_private')
            epoch_time = data.get('ts')
            ts         = datetime.datetime.fromtimestamp(float(epoch_time))

            if [user, ts, url_path] not in all_urls_in_this_json:
                all_urls_in_this_json.append([user, ts, url_path])

    return all_urls_in_this_json

def get_jsonpaths():
    filepath_skack_export = os.path.join(json_reader.config.FOLDERPATH_INPUT_FILES, json_reader.config.FILENAME_SLACK_EXPORT)

    paths_to_channels = [os.path.join(filepath_skack_export, chnl) for chnl in json_reader.config.CHANNELS]

    all_jsons = []

    for p in paths_to_channels:
        jsons       = os.listdir(p)
        jsons_paths = [os.path.join(p, f) for f in jsons]

        for j in jsons_paths:
            all_jsons.append(j)

    return all_jsons

def main():

    # Get all the filepaths from the the channels I like
    json_filepaths = get_jsonpaths()[:]


    # Save the filepaths so I can look at them
    with open(os.path.join(json_reader.config.FOLDERPATH_INTERMEDIATE_FILES, 'json_filepaths.txt'), 'w') as save_path:
        for filepath in json_filepaths:
            f_str = str(filepath)
            save_path.write(f_str)
            save_path.write('\n')

    # Read the data from the json_filepaths and get [user, timestamp, link] for each file
    web_urls = []
    for save_path in json_filepaths:
        all_urls = get_links_within_json(save_path)
        for u in all_urls:
            web_urls.append(u)


    # Save all the download links
    with open(os.path.join(json_reader.config.FOLDERPATH_INTERMEDIATE_FILES, 'download_links.csv'), 'w') as save_path:
        for filepath in web_urls:
            user_name = filepath[0]
            timestamp = filepath[1]
            fp        = filepath[2]
            f_str     = str(fp)
            save_path.write(f_str)
            save_path.write('\n')

    for idx, a in enumerate(web_urls):

        filename = '{:%Y_%m_%d__%H_%M_%S}__{}.txt'.format(a[1], a[0])
        save_path = os.path.join(json_reader.config.FOLDERPATH_INTERMEDIATE_FILES, 'url_downloads', filename)

        with urllib.request.urlopen(a[2]) as url:
            s = url.read()

        if len(s) < 10000:
            with open(save_path, 'wb') as ff:
                ff.write(s)
#
#

if __name__ == '__main__':
    main()