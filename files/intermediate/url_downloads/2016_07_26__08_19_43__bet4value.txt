PCMAC::Tue Jul 26 08:19:40 2016
DEP VAR CRUDE AT 900 ALL DAYS: 1/1/2009 to 7/22/2016
AS OF 1 1430, CRUDE AT 20 COMPARISON DAY LOW 
AS OF 2 1430, CRUDE AT 20 COMPARISON DAY LOW 
INDEPENDNT   LAG1 HOUR   LAG2 HOUR       LO       HI     (TRADING DAYS)
     CRUDE     1  1430     0   900    -5000      -20
     CRUDE     2  1430     1   900    -5000      -20

   Y  M  D  DW  PLAST  910  920  930 1000 1100 1200 1300 1400 1500 1600 1700 1720
---------------------------------------------------------------------------------
2015  3 18 WED   5866   -3   -5    4  -13    5   -3   36   46  236  306  254  262
2015  7  6 MON   6643    6   -2   11  -26  -27  -42  -90 -133 -200 -167 -185 -186
2015  7 20 MON   6250    0    3    1  -30    0  -25  -27  -53  -65  -68  -70  -69
2015  8  7 FRI   5591  -17  -20   -5  -13   -6   -5  -24  -23  -49  -50  -53  -59
2015 10 27 TUE   5332  -27  -23  -21  -21    1  -12  -28   -5    6   14   30   30
2015 11 13 FRI   5150   26   14  -14  -47  -58  -47  -60  -75  -53  -51  -54  -54
2015 12 11 FRI   4543   22    8    8  -36  -36  -43  -73  -58  -59  -76  -87  -87
2015 12 14 MON   4448   -4   -1    3   -5   66   92  126   90  110   95   99   99
2015 12 21 MON   4368  -24   -7  -12  -17    3    1   -4    4    8    9    8    8
2016  1 21 THR   3519  -16    9    8    8   27  172  185  163  166  163  173  173

D HOUR     N   MAX   MIN    MU   MUD  PPOS  SDEV     T  TDRF  SER
0  910    40    62   -29    10    10    65    22   279   289** 43
0  920    40   120   -54    13    13    63    34   234   242    8
0  930    40   104   -54    16    16    65    37   269   272** 22
0 1000    40   171  -106     2     3    45    57    19    37    6
0 1100    40   200  -228    -2    -0    45    83   -15    -3  -18
0 1200    40   374  -328    11    13    48   120    58    66   -6
0 1300    40   328  -358     8     8    50   113    42    46   -5
0 1400    40   281  -358     9     9    55   110    54    52   -9
0 1500    40   252  -358     6     5    58   121    33    26  -14
0 1600    40   306  -358    17    16    55   130    85    78   -7
0 1700    40   296  -358    16    13    58   135    73    63   -4
0 1720    40   312  -358    16    13    55   138    72    61   -5
-----------------------------------------------------------------
1  100    40   276  -435     6     5    53   152    23    19  -26
1 1720    40   419  -654    13    14    65   234    36    37   -9


----------------------HI.LO THRU  0 1720--------------------
SCENARIO:                         N     MU   PPOS  SDEV   T 
------------------------------------------------------------
ENTER    1 BELO OPEN TO CLOSE:    32     5    47   139    21
ENTER    5 BELO OPEN TO CLOSE:    32     7    47   138    28
ENTER   10 BELO OPEN TO CLOSE:    31     7    45   139    29
ENTER   20 BELO OPEN TO CLOSE:    27   -16    44   118   -72
ENTER   50 BELO OPEN TO CLOSE:    22     1    45   110     3
ENTER   75 BELO OPEN TO CLOSE:    17     9    59   121    32
ENTER  100 BELO OPEN TO CLOSE:    15    25    60   123    80
ENTER   AT YEST CLOS TO CLOSE:     2     2    50   154     2
ENTER   AT YEST  LOW TO CLOSE:     9    64    67    95   201
ENTER   AT INTRDY LO TO CLOSE:    26   -10    42   117   -43
------------------------------------------------------------
ENTER   1 ABOVE OPEN TO CLOSE:    36    16    56   131    75
ENTER   5 ABOVE OPEN TO CLOSE:    35    16    54   132    71
ENTER  10 ABOVE OPEN TO CLOSE:    33    27    58   115   136
ENTER  20 ABOVE OPEN TO CLOSE:    30    35    63   107   178
ENTER  50 ABOVE OPEN TO CLOSE:    22    40    73    97   192
ENTER  75 ABOVE OPEN TO CLOSE:    19    25    58    88   123
ENTER 100 ABOVE OPEN TO CLOSE:    16    18    50    82    89
ENTER  AT  YEST CLOS TO CLOSE:    16    68    75   107   253
ENTER  AT  YEST HIGH TO CLOSE:     6   -53    67   129  -101
ENTER  AT  INTRDY HI TO CLOSE:    18    38    67   100   162
