PCMAC::Tue Sep 06 08:27:26 2016
DEP VAR SP AT 930 TUE: 1/1/2009 to 8/26/2016
HOLIDAY AT TRADEDAY -1

   Y  M  D  DW  PLAST  940  950 1000 1100 1200 1300 1400 1500 1600 1620  930 1620
---------------------------------------------------------------------------------
2014  5 27 TUE  18322   -6   15   12   35   18   12   15   25   40   40   40   37
2014  9  2 TUE  19387  -32  -30  -27  -15  -72 -102  -88  -50  -40  -40   42  -55
2015  1 20 TUE  19683  -30 -125 -135 -165 -200 -160 -125  -40  -65  -35  -90   70
2015  2 17 TUE  20378  -23  -12   -2   -2   25   48   65   52   68   62   20   57
2015  5 26 TUE  20732  -42  -70  -90 -150 -150 -170 -207 -218 -160 -132 -112   28
2015  9  8 TUE  19147   -5   45    0  -31  -55    2   25   87  153  132  340  -95
2016  1 19 TUE  18757  -20 -115  -90 -120  -97 -190 -195 -322 -205 -210 -500 -395
2016  2 16 TUE  18647  -25  -40  -52  -47    3   55   57   72   78   58  230  340
2016  5 31 TUE  20900  -12    3   -5  -25  -70  -77  -70 -120  -50  -45 -140  -16
2016  7  5 TUE  20845   -5  -60  -40  -57  -85  -65  -92  -90  -50  -15 -105   90

D HOUR     N   MAX   MIN    MU   MUD  PPOS  SDEV     T  TDRF  SER
0  940    36    60   -42    -1    -2    42    22   -39   -57    6
0  950    36    69  -125    -6    -5    53    47   -76   -68   -2
0 1000    36    92  -145    -7    -7    36    52   -86   -87    7
0 1100    36   242  -165    -5    -6    44    77   -41   -45   11
0 1200    36   252  -200   -16   -17    44    83  -120  -123   12
0 1300    36   292  -190   -18   -20    44    95  -117  -125   -0
0 1400    36   302  -231   -16   -18    42   105   -90  -101   -4
0 1500    36   278  -322   -18   -21    47   115   -93  -112  -13
0 1600    36   302  -368    -7   -11    44   119   -35   -54  -13
0 1620    36   297  -328    -1    -5    47   115    -3   -26  -10
-----------------------------------------------------------------
1  930    36   435  -500    -6   -14    42   160   -23   -51  -36
1 1620    36   610  -395     6    -6    50   179    19   -21   -0
-----------------------------------------------------------------
2  930    36   518  -370    18     3    50   184    58     9    5
2 1620    36   481  -330    40    20    56   199   120    61    3


----------------------HI.LO THRU  0 1620--------------------
SCENARIO:                         N     MU   PPOS  SDEV   T 
------------------------------------------------------------
ENTER    1 BELO OPEN TO CLOSE:    31   -11    45    88   -70
ENTER    5 BELO OPEN TO CLOSE:    30   -13    43    88   -82
ENTER   10 BELO OPEN TO CLOSE:    29    -9    48    92   -54
ENTER   20 BELO OPEN TO CLOSE:    29    -2    52    93   -13
ENTER   50 BELO OPEN TO CLOSE:    21     8    52    81    45
ENTER   75 BELO OPEN TO CLOSE:    17    17    65    82    84
ENTER  100 BELO OPEN TO CLOSE:    12    18    58    94    68
ENTER   AT INTRDY LO TO CLOSE:    31   -11    45    88   -70
------------------------------------------------------------
ENTER   1 ABOVE OPEN TO CLOSE:    28    14    54    93    77
ENTER   5 ABOVE OPEN TO CLOSE:    27    11    52    96    57
ENTER  10 ABOVE OPEN TO CLOSE:    27    10    52    96    56
ENTER  20 ABOVE OPEN TO CLOSE:    27     4    52    97    20
ENTER  50 ABOVE OPEN TO CLOSE:    17     3    59   111     9
ENTER  75 ABOVE OPEN TO CLOSE:    12    15    58   101    50
ENTER 100 ABOVE OPEN TO CLOSE:     5    22    80   136    36
ENTER  AT  INTRDY HI TO CLOSE:    28    14    54    93    77


