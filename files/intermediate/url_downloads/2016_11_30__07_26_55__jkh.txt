JKH::Wed 30 Nov 2016 07:26:37 AM EST
DEP VAR SP AT 800 : 01/01/2013 TO 01/01/2020  US/Eastern

INDEPENDNT LAG1  HOUR LAG2 HOUR  LO     HI          TZ
    tbond    1  1715    0  800  -1  -5000  US/Eastern
       sp    1  1615    0  800   1   5000  US/Eastern
LOOKING AT DAY 1 FROM END OF MONTH



LAG  time  MU    T   TRDF    STD  PPOS  COR   N
  0   805   1   25    25      11    40    0  10
  0   810   3   82    82       9    50   -8  10
  0   815   2   81    81       7    50   -6  10
  0   820   2   86    86       9    40  -12  10
  0   825   2   73    73      10    40   19  10
  0   830   0   11    11       8    40   20  10
  0   900  -1  -15   -18      18    30    1  10
  0   930  -1  -14   -19      27    40    8  10
  0  1000 -30 -167  -170      56    40   16  10
  0  1100 -16  -85   -90      59    40  -32  10
  0  1200 -28 -135  -142      64    40    0  10
  0  1300 -42 -202  -209*     65    20   37  10
  0  1400 -33 -170  -180      60    30   10  10
  0  1500 -33 -156  -167      66    30  -18  10
  0  1600 -24  -85   -94      90    50  -43  10
  0  1615 -25 -103  -114      75    30  -21  10
  0  1715 -27 -108  -120      79    30   -7  10