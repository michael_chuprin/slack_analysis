PCMAC::Thu Oct 06 08:47:11 2016
DEP VAR NITE.SP AT 1520 ALL DAYS: 1/1/2013 to 9/30/2016
INCLUDE UNEMPLOYMENT ANNOUNCEMENT AT T(1)

   Y  M  D  DW  PLAST 1530 1540 1550 1600 1620  100  200  300  400  500  600 1620
---------------------------------------------------------------------------------
2015 12  3 THR  20092    3   53   50   58   83  123  120  156  186  166  178  453
2016  1  7 THR  19118  -68   30   47  -23  -53  127  154  180  217  204  142 -256
2016  2  4 THR  18788    2   24   42   54   34   12    4   12   22   84   72 -290
2016  3  3 THR  19610   15   28   45   50   45   38   40   38   22   35   50   82
2016  3 31 THR  20350   18   22   30   -2   25  -45  -42  -48  -75  -38  -62  142
2016  5  5 THR  20262   10    3   -2   20   16  -10   -7  -10    3   13  -20  103
2016  6  2 THR  20830   22   28    5   40   42   38   40   38   32   42   40  -15
2016  7  7 THR  20805    3   25   30   33   50   17   23   33   75   57   87  327
2016  8  4 THR  21518    2   12   14   14    4   34   37   37   50   40   57  182
2016  9  1 THR  21570    8   10   30   50   32   22   30   40   35   35   45  142

D HOUR     N   MAX   MIN    MU   MUD  PPOS  SDEV     T  TDRF  SER
0 1530    45    34   -68     0    -1    58    18    16   -33    9
0 1540    45    53   -63     7     4    62    23   193   111   29
0 1550    45    56   -34     9     7    53    22   279   203   19
0 1600    45    84   -36    13    12    64    25   359   336**-23
0 1620    45   102   -56    16    15    71    31   346   315**-22
-----------------------------------------------------------------
1  100    45   127  -198    19    18    76    47   271   254**  4
1  200    45   154  -193    22    21    78    50   299   274**  7
1  300    45   180  -118    28    23    80    49   383   317** 14
1  400    45   217  -160    29    24    80    57   341   280** 18
1  500    45   204  -226    29    24    76    65   300   245    7
1  600    45   178  -238    24    19    69    68   234   188    4
1 1620    45   453  -300    51    41    64   169   204   165    1
-----------------------------------------------------------------
2  100    45   433  -280    57    48    69   167   230   191    8
2 1620    45   650  -518    54    35    69   214   168   111   -8


----------------------HI.LO THRU  0 1620--------------------
SCENARIO:                         N     MU   PPOS  SDEV   T 
------------------------------------------------------------
ENTER    1 BELO OPEN TO CLOSE:    25    16    76    25   323
ENTER    5 BELO OPEN TO CLOSE:    21    16    76    26   282
ENTER   10 BELO OPEN TO CLOSE:    16    16    81    26   241
ENTER   20 BELO OPEN TO CLOSE:    12    22    75    29   260
ENTER   50 BELO OPEN TO CLOSE:     3    49   100    58   148
ENTER   AT YEST CLOS TO CLOSE:     3     4    67     7   101
ENTER   AT INTRDY LO TO CLOSE:     5    13   100     4   829
------------------------------------------------------------
ENTER   1 ABOVE OPEN TO CLOSE:    39     9    51    29   183
ENTER   5 ABOVE OPEN TO CLOSE:    36     6    53    26   133
ENTER  10 ABOVE OPEN TO CLOSE:    34     6    53    26   128
ENTER  20 ABOVE OPEN TO CLOSE:    24     6    63    29   103
ENTER  50 ABOVE OPEN TO CLOSE:     9     0    56    19     0
ENTER  75 ABOVE OPEN TO CLOSE:     2     1    50    20     3
ENTER 100 ABOVE OPEN TO CLOSE:     2   -14     0     4  -472
ENTER  AT  YEST CLOS TO CLOSE:     2   -12     0     4  -402
ENTER  AT  YEST HIGH TO CLOSE:     4    11    75    24    86
ENTER  AT  INTRDY HI TO CLOSE:     8    10    50    30    88


