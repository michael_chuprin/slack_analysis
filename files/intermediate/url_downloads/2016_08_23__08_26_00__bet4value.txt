PCMAC::Tue Aug 23 08:25:41 2016
DEP VAR NITE.SP AT 930 ALL DAYS: 1/1/2013 to 8/12/2016
AS OF 0 930, NITE.SP AT 20 COMPARISON DAY HIGH 
INDEPENDNT   LAG1 HOUR   LAG2 HOUR       LO       HI     (TRADING DAYS)
   NITE.SP     2  1620     1  1620      -65       -1
   NITE.SP     1  1620     0   930        1     5000

   Y  M  D  DW  PLAST  940  950 1000 1100 1200 1300 1400 1500 1600 1620  100 1620
---------------------------------------------------------------------------------
2014 11 13 THR  19770   32   40   42   50   -8  -18  -58  -48    2  -28   -8   20
2014 12  5 FRI  20135   15   13   20    5   50   47   37    7   17   35   25 -130
2014 12 26 FRI  20308   14   32   40   32   32   32   42   50    4   12   27   30
2015 10 12 MON  19802  -22   -4  -24  -34   10    0  -10   -2   13   36    6 -134
2015 10 21 WED  20025  -30  -10  -17  -50  -30  -53 -137  -87 -180 -213 -157  223
2015 10 30 FRI  20572   -4  -24  -40  -32  -30   18   18  -62 -120 -102 -190  106
2016  2 22 MON  19142   50   58   66   48   63   43   98   60   88   23  -34 -174
2016  7  8 FRI  21065  -10   27   27   85  107  113  135  167  160  135  217  240
2016  7 14 THR  21605  -20  -47  -15  -17  -30  -30  -60  -30  -37  -30  -57  -80
2016  7 20 WED  21638  -36  -36  -26   34   40   32   47   40   32   34   32  -53

D HOUR     N   MAX   MIN    MU   MUD  PPOS  SDEV     T  TDRF  SER
0  940    20    50   -43    -2    -3    40    26   -30   -52    9
0  950    20    63   -75    -2    -2    40    36   -20   -22    1
0 1000    20    66   -67     1     0    45    39    15     1  -13
0 1100    20    85   -92     4     2    55    48    34    20  -16
0 1200    20   108  -100    10     8    60    55    83    61  -26
0 1300    20   113  -124     8     4    55    54    65    36  -28
0 1400    20   135  -142     3    -0    55    74    18    -3  -17
0 1500    20   167  -127     6     1    45    72    38     5  -19
0 1600    20   160  -180     4    -2    65    82    22    -9   -3
0 1620    20   135  -213    -1    -7    55    78    -5   -41   -7
-----------------------------------------------------------------
1  100    20   217  -190    -4   -10    50    96   -19   -47  -11
1 1620    20   240  -224     9    -6    60   128    33   -21  -52
-----------------------------------------------------------------
2  100    20   267  -252     1   -14    55   138     3   -46  -41
2 1620    20   395  -232    49    25    60   151   146    74  -19


----------------------HI.LO THRU  0 1620--------------------
SCENARIO:                         N     MU   PPOS  SDEV   T 
------------------------------------------------------------
ENTER    1 BELO OPEN TO CLOSE:    16     5    63    79    27
ENTER    5 BELO OPEN TO CLOSE:    15     2    60    76     8
ENTER   10 BELO OPEN TO CLOSE:    15     4    60    77    18
ENTER   20 BELO OPEN TO CLOSE:    13   -10    62    70   -52
ENTER   50 BELO OPEN TO CLOSE:     9    -6    56    69   -26
ENTER   75 BELO OPEN TO CLOSE:     7    -2    43    62    -7
ENTER  100 BELO OPEN TO CLOSE:     4   -12    75    39   -61
ENTER   AT YEST CLOS TO CLOSE:     8    -9    50    62   -42
ENTER   AT YEST  LOW TO CLOSE:     3   -22    33    42   -89
ENTER   AT INTRDY LO TO CLOSE:     8   -11    50    64   -50
------------------------------------------------------------
ENTER   1 ABOVE OPEN TO CLOSE:    16     6    50    55    47
ENTER   5 ABOVE OPEN TO CLOSE:    15     6    53    54    41
ENTER  10 ABOVE OPEN TO CLOSE:    15     2    53    53    13
ENTER  20 ABOVE OPEN TO CLOSE:    14     2    57    55    10
ENTER  50 ABOVE OPEN TO CLOSE:    10    -4    40    44   -28
ENTER  75 ABOVE OPEN TO CLOSE:     4     5    75    40    24
ENTER 100 ABOVE OPEN TO CLOSE:     4   -16    25    39   -80
ENTER  AT  YEST HIGH TO CLOSE:     9   -14    33    52   -84
ENTER  AT  INTRDY HI TO CLOSE:    10    19    50    42   146


