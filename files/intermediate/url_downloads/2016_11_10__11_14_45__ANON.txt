JKH::Thu 10 Nov 2016 11:12:45 AM EST
DEP VAR SP AT 1200 : 01/01/2013 TO 01/01/2020  US/Eastern

INDEPENDNT LAG1  HOUR LAG2  HOUR    LO     HI          TZ
       sp    1  1615    0  1200  -100  -5000  US/Eastern
       sp    2  1615    1  1615   200   5000  US/Eastern



LAG  time  MU    T   TRDF    STD  PPOS  COR   N
  0  1205  -7 -161  -161      17    41    4  17
  0  1210  -5  -99   -98      20    41   16  17
  0  1215  -4  -51   -51      34    41    4  17
  0  1220  -7  -80   -81      37    41    2  17
  0  1225 -15 -150  -151      39    35   10  17
  0  1230 -16 -156  -157      42    35   32  17
  0  1300 -24 -229  -232*     42    29    6  17
  0  1400 -17 -154  -159      46    23   20  17
  0  1500 -31 -171  -176      75    35   32  17
  0  1600 -50 -203  -209*    100    41    0  17
  0  1615 -50 -172  -177     118    47  -10  17
  0  1700 -55 -178  -184     125    47  -16  17
  0  1715 -57 -188  -193     125    47  -14  17