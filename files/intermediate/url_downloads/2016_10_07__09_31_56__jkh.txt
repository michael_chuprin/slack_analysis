JKH::Fri 07 Oct 2016 09:31:44 AM EDT
DEP VAR SP AT 930 : 01/01/2013 TO 01/01/2020  US/Eastern

INDEPENDNT LAG1  HOUR LAG2 HOUR LO    HI          TZ
       sp    1  1615    0  930  1  5000  US/Eastern
AS OF 1 1715 US/EASTERN, TBOND AT 10 COMPARISON DAY MIN



LAG  time  MU    T   TRDF    STD  PPOS  COR   N
  0   935   1   26    23      15    45    6  59
  0   940  -1  -45   -49      21    42   -2  59
  0   945  -5 -104  -108      34    37  -22  59
  0   950  -3  -57   -60      40    44   -9  59
  0   955  -1  -20   -23      45    45  -11  59
  0  1000   2   26    23      46    52  -10  59
  0  1100   8  102    96      59    45  -16  59
  0  1200  16  188   178      65    57   -3  59
  0  1300  23  233   220      76    57   -8  59
  0  1400  27  247   231      83    57   -8  59
  0  1500  28  241   223      88    57  -12  59
  0  1600  30  245   225      93    57   -8  59
  0  1615  33  274   254      93    61   -9  59
  0  1700  34  281   258      92    61  -12  59
  
  
  JKH::Fri 07 Oct 2016 09:44:32 AM EDT
DEP VAR SP AT 930 : 01/01/2013 TO 01/01/2020  US/Eastern

INDEPENDNT LAG1  HOUR LAG2 HOUR LO    HI          TZ
       sp    1  1615    0  930  1  5000  US/Eastern
AS OF 1 1715 US/EASTERN, TBOND AT 10 COMPARISON DAY MIN



  STARTING FROM 930 HOLDING TILL 1700
          N  ppos  mean  tscore
100      17    58    42     218
50       33    57    29     201
20       45    62    31     227
10       51    58    31     245
1        52    61    33     271
-----------------------------
             N  ppos  mean  tscore
-1          49    57    25     228
-10         47    59    31     265
-20         43    65    35     286
-50         27    74    41     264
-100         9    88    82     187
previous    34    73    33     286